**Code Samples**


*We recommend that you review python, xml and Js files added in this repository.*

---

## Python

Refer *pricelist.py* file for Additional product price attributes.

---

## XML Template


Website Template to design and display building blogs on Odoo Website.

---

## Javascript

JS file to add Date range filter bar in list views.
