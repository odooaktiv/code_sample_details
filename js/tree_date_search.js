//  -*- coding: utf-8 -*-
//  License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

odoo.define('web_tree_date_search.ListView', function(require) {
"use strict";

    var core = require('web.core');
    var _t = core._t;

    var QWeb = core.qweb;

    var ListController = require("web.ListController");
    var ListRenderer = require('web.ListRenderer');
    var datepicker = require('web.datepicker');

    ListRenderer.include({
        init: function(parent, state, params) {
            this._super.apply(this, arguments);
            var field = params.arch.attrs.date_search || false;
            if (field && state.fields[field]) {
                var type = state.fields[field].type;
                if (_.contains(["date", "datetime"], type)) {
                    this.date_field = field;
                    this.date_field_type = type;
                }
            }
        },

        start: function() {
            var self = this;
            this.$filter_bar = $('<div>', {class: 'o_filter_bar'})[0];
            this.$el[0].before(this.$filter_bar);

            if (this.date_field) {
                var cls = this.date_field_type == 'datetime' ? datepicker.DateTimeWidget : datepicker.DateWidget;
                this.$start_picker = this._init_date_picker(cls, _t("From"));
                this.$end_picker = this._init_date_picker(cls, _t("To"));
            }
            var parent_model = self.__parentedParent && self.__parentedParent.modelName;
            if (parent_model == 'pos.top.sellers.shop.report'){
                var d = moment().format('YYYY-MM-DD');
                var date_change_check = false;
                if (this.$start_picker != undefined){
                    this.$start_picker.setValue(moment.utc(d + " 07:00:00"));
                    date_change_check = true;
                }
                if (this.$end_picker != undefined){
                    this.$end_picker.setValue(moment.utc(d + " 23:30:00"));
                    date_change_check = true;
                }
                if (date_change_check){
                    self._onDateSearchChange();
                }
            }
            return this._super.apply(this, arguments);
        },

        _init_date_picker: function(cls, text) {
            var self = this;
            var picker = new cls(this);
            picker.appendTo(this.$filter_bar);
            picker.$input.addClass("o_date_search_input");
            picker.$input.attr("placeholder", text);
            picker.on('datetime_changed', self, self._onDateSearchChange);
            return picker;
        },

        _onDateSearchChange: function() {
            this.trigger_up('date_filter_change', {
                field: this.date_field,
                from: this.$start_picker.getValue(),
                to: this.$end_picker.getValue(),
            });
        },
    });

    ListController.include({
        custom_events: _.extend({}, ListController.prototype.custom_events || {}, {
            "date_filter_change": "_onDateSearchChange",
        }),

        domain_update: function(field, domain, op, value) {
            if (!value)
                return false;
            for (var i = 0; i < domain.length; i++) {
                var d = domain[i];
                if (d.length != 3 || d[0] != field || d[1] != op)
                    continue;
                else if (d[2] == value)
                    return false;
                else {
                    d[2] = value;
                    return true;
                }
            }

            domain.push([field, op, value]);
            return true;
        },

        _onDateSearchChange: function(event) {
            var self = this;
            if (!event.data.field)
                return;

            function to_local_timestamp(value, date_field_type) {
                if (date_field_type == 'datetime'){
                    // value from datepicker is judged as UTC by default but it is local time in real
                    // so following line is used to correct that and treat the user input as local time not UTC
                    return value && moment(value.utc().format('YYYY-MM-DD HH:mm:ss')) || false;
                }else{
                    return value || false;
                }
            }

            function isSame(d1, d2) {
                if( typeof(d1) === 'undefined' || d1 === false || typeof(d2) === 'undefined' || d2 === false ) {
                    return d1 === d2;
                }
                return d1.isSame(d2);
            }

            var p = new Promise(function(resolve,reject){
                var from = to_local_timestamp(event.data.from, self.renderer.date_field_type);
                var to = to_local_timestamp(event.data.to, self.renderer.date_field_type);

                if( isSame(self._last_from, from) && isSame(self._last_to, to) )
                    return;

                self._last_from = from;
                self._last_to = to;
                var field = event.data.field;
                var domain = self.renderer.state.domain.length ? self.renderer.state.domain : [];
                if (self.domain_update(field, domain, '>=', from)){
                    self.renderer.state.domain = domain;
                }
                if (self.domain_update(field, domain, '<=', to)){
                    self.renderer.state.domain = domain;
                }
                resolve();
            });

            p.then(function(status){
              self.update(self.renderer.state, {'keepSelection': true});
            });
        },
    });
});
